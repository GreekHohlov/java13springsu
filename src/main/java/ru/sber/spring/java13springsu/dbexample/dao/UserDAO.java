package ru.sber.spring.java13springsu.dbexample.dao;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.sber.spring.java13springsu.model.User;

import java.sql.*;

@Component
@Scope("prototype")
public class UserDAO {
    private final Connection connection;

    public UserDAO(Connection connection){
        this.connection = connection;
    }
    //Метод для создания пользователя
    public void userAdd(User user) throws SQLException {
        PreparedStatement insertQuery = connection.prepareStatement("insert into client (last_name, first_name," +
                "birth_date, phone, email, book_list) VALUES (?, ?, ?, ?, ?, ?)");
        insertQuery.setString(1, user.getLastName());
        insertQuery.setString(2, user.getFirstName());
        insertQuery.setDate(3, Date.valueOf(user.getBirthDate()));
        insertQuery.setString(4, user.getPhone());
        insertQuery.setString(5, user.getEmail());
        insertQuery.setString(6, user.getBookList());
        insertQuery.executeUpdate();
    }

    //Полученин списка книг пользователя
    public String[] getUserBooks(String phone) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("select book_list from client where phone = ?");
        selectQuery.setString(1, phone);
        ResultSet resultSet = selectQuery.executeQuery();
        while (resultSet.next()){
            String result = resultSet.getString(1);
            System.out.println(result);
            return result.split("; ");
        }
        return null;
    }
}
