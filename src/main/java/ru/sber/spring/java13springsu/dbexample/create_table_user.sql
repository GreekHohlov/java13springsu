
create table client (
    id serial primary key,
    last_name varchar(30) not null,
    first_name varchar(30) not null,
    birth_date date not null,
    phone varchar(11) not null,
    email varchar(20) not null,
    book_list varchar(300) not null
);

drop table client;