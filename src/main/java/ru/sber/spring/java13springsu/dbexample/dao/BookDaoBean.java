package ru.sber.spring.java13springsu.dbexample.dao;

import org.springframework.stereotype.Component;
import ru.sber.spring.java13springsu.dbexample.model.Book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class BookDaoBean {
    private final Connection connection;

    public BookDaoBean(Connection connection){
        this.connection = connection;
    }
    public Book findBookById(Integer bookId) throws SQLException{
        PreparedStatement selectQuery = connection.prepareStatement("select * from books where id = ?");
        selectQuery.setInt(1, bookId);
        ResultSet resultSet = selectQuery.executeQuery();
        Book book = new Book();
        while (resultSet.next()){
            book.setAuthor(resultSet.getString("author"));
            book.setBookTitle(resultSet.getString("title"));
            book.setDateAdded(resultSet.getDate("date_added"));
        }
        return book;
    }

    //Получение информации о книге по названию
    public List<Book> findBookByTitle(String[] books) throws SQLException {
        List<Book> list = new ArrayList<>();
        for (String element: books){
            PreparedStatement selectQuery = connection.prepareStatement("select * from books " +
                    "where title = ?");
            selectQuery.setString(1, element);
            ResultSet resultSet = selectQuery.executeQuery();
            Book book = new Book();
            while (resultSet.next()){
                book.setAuthor(resultSet.getString("author"));
                book.setBookTitle(resultSet.getString("title"));
                book.setDateAdded(resultSet.getDate("date_added"));
                list.add(book);
            }
        }
        return list;
    }
}
