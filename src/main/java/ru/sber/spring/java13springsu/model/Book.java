package ru.sber.spring.java13springsu.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "books")
@Getter
@Setter
@NoArgsConstructor
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "books_gen")
    @SequenceGenerator(name = "books_gen", sequenceName = "books_seq")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "publish_date")
    private LocalDate publishDate;

    @Column(name = "publish")
    private String publish;

    @Column(name = "page_count")
    private Integer pageCount;

    @Column(name = "storage_place")
    private String storagePlace;

    @Column(name = "online_copy_path")
    private String onlineCopy;

    @Column(name = "description")
    private String description;

    @Column(name = "amount")
    private Integer amount;

    @Column(name = "genre")
    @Enumerated
    private Genre genre;
    @ManyToMany
    @JoinTable(name = "books_authors",
            joinColumns = @JoinColumn(name = "book_id"), foreignKey = @ForeignKey(name = "FK_BOOKS_AUTHOR"),
    inverseJoinColumns = @JoinColumn(name = "author_id"), inverseForeignKey = @ForeignKey(name = "FK_AUTHORS+BOOKS"))
    private Set<Author> authors;
}