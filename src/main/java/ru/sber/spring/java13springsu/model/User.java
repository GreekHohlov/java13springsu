package ru.sber.spring.java13springsu.model;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    private Integer id;
    private String lastName;
    private String firstName;
    private LocalDate birthDate;
    private String phone;
    private String email;
    private String bookList;
}
