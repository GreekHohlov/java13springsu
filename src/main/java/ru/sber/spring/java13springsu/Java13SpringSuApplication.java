package ru.sber.spring.java13springsu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.sber.spring.java13springsu.dbexample.MyDBConfigContext;
import ru.sber.spring.java13springsu.dbexample.dao.BookDaoBean;
import ru.sber.spring.java13springsu.dbexample.dao.BookDaoJDBC;
import ru.sber.spring.java13springsu.dbexample.dao.UserDAO;
import ru.sber.spring.java13springsu.model.User;

import java.time.LocalDate;


@SpringBootApplication
public class Java13SpringSuApplication implements CommandLineRunner {
    private BookDaoBean bookDaoBean;
    private final UserDAO userDAO;
    private NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
    @Autowired
    public void setBookDaoBean(BookDaoBean bookDaoBean) {
        this.bookDaoBean = bookDaoBean;
    }
    public Java13SpringSuApplication(BookDaoBean bookDaoBean, UserDAO userDAO) {
        this.bookDaoBean = bookDaoBean;
        this.userDAO = userDAO;
    }

    public static void main(String[] args) {

        SpringApplication.run(Java13SpringSuApplication.class, args);

    }

    @Override
    public void run(String... args) throws Exception {
        BookDaoJDBC bookDaoJDBC = new BookDaoJDBC();
        bookDaoJDBC.findBookById(1);

        ApplicationContext ctx = new AnnotationConfigApplicationContext(MyDBConfigContext.class);
        BookDaoBean bookDaoBean = ctx.getBean(BookDaoBean.class);
        System.out.println(bookDaoBean.findBookById(1));

        //Добавление пользователей
        User userOne = new User(1, "Петров", "Иван",
                LocalDate.of(1987, 12, 5),"79137842113", "petrov.ivan@bk.ru",
                "Доктор Живаго; Путешествие из Петербурга в Москву");
        userDAO.userAdd(userOne);
        User userTwo = new User(2, "Семёнов", "Дмитрий",
                LocalDate.of(1963, 7, 13), "79624536789", "semenov@gmail.com",
                "Сестра моя - жизнь;");
        userDAO.userAdd(userTwo);
        //Получить список книг пользователя по номеру телефона
        String[] bookOfUser = userDAO.getUserBooks("79137842113");
        //Вывод информации о книгах
        System.out.println(bookDaoBean.findBookByTitle(bookOfUser));
    }
}
