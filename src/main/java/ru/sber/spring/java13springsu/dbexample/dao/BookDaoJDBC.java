package ru.sber.spring.java13springsu.dbexample.dao;

import ru.sber.spring.java13springsu.dbexample.db.DbApp;
import ru.sber.spring.java13springsu.dbexample.model.Book;

import java.sql.*;

public class BookDaoJDBC {

    public Book findBookById(Integer bookId){
        try (Connection connection = DbApp.INSTANCE.getConnection()){
            if (connection != null){
                System.out.println("Ура! Я подключился к базе!!!");
            } else {
                System.out.println("База данных отдыхает!");
            }
            PreparedStatement selectQuery = connection.prepareStatement("select * from books where id = ?");
            selectQuery.setInt(1, bookId);
            ResultSet result = selectQuery.executeQuery();
            while (result.next()){
                Book book = new Book();
                book.setBookTitle(result.getString("title"));
                book.setAuthor(result.getString("author"));
                book.setDateAdded(result.getDate("date_added"));
                System.out.println(book);
                return book;
            }
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }

//    private Connection getConnection() throws SQLException {
//        return DriverManager.getConnection(
//                "jdbc:postgresql://localhost:5432/postgres",
//                "postgres",
//                "12345");
//    }

}
